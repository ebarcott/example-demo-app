FROM node:11.13
#ENV NODE_ENV=production
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
COPY pkg/next-demo-0.1.0.tgz .

COPY www/package*.json ./
USER node
RUN npm install
RUN npm install next-demo-0.1.0.tgz
COPY --chown=node:node www ./

ENTRYPOINT ["npm", "start"]
