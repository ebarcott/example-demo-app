import { what_lang as guessLang } from 'next-demo';

const form = document.getElementById('language-form');
const input = form.querySelector('.input');
const output = form.querySelector('.output');

input.addEventListener('input', event => {
  if (!input.value.trim()) {
    output.textContent = '';
    return;
  }

  output.textContent = guessLang(input.value);
});

form.addEventListener('submit', event => {
  event.preventDefault();
  output.textContent = guessLang(input.value);
});
