mod utils;

use wasm_bindgen::prelude::*;
extern crate whatlang;
use whatlang::{detect};


// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub fn what_lang(s: &str) -> String {
    let info = detect(s).unwrap();
    info.lang().eng_name().to_string()
}
